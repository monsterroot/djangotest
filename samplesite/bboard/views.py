from django.http.request import HttpRequest
from django.shortcuts import render
from django.http import HttpResponse
from django.template import context, loader
from django.shortcuts import render


from .models import Bb, Rubric
# Create your views here.

def index(request):
    # template = loader.get_template('index.html')
    # bbs = Bb.objects.order_by('-published')
    # context = {'bbs':bbs}
    # return HttpResponse(template.render(context, request))
    bbs = Bb.objects.order_by('-published')
    rubrics = Rubric.objects.all()
    return render(request, 'bstest.html', {'bbs': bbs, 'rubrics': rubrics})

def by_rubric(request, rubric_id):
    bbs = Bb.objects.filter(rubric=rubric_id)
    rubrics = Rubric.objects.all()
    current_rubric = Rubric.objects.get(pk=rubric_id)
    context = {'bbs': bbs, 'rubrics': rubrics,
        'current_rubric': current_rubric}
    return render(request, 'by_rubric.html', context)