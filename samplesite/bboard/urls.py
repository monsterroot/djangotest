from django.urls import path

from .views import index, by_rubric

urlpatterns = [
    # , name='by_rubric'
    path('<int:rubric_id>/', by_rubric),
    path('', index, name='index'),
]